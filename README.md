# Build Log

### 1/4/2022

When building android hal, errors like this pop:
```
[100% 182/182] out/soong/.bootstrap/bin/soong_build out/soong/build.ninja
FAILED: out/soong/build.ninja
out/soong/.bootstrap/bin/soong_build -t -l out/.module_paths/Android.bp.list -b out/soong -n out -d out/soong/build.
ninja.d -globFile out/soong/.bootstrap/build-globs.ninja -o out/soong/build.ninja Android.bp
error: packages/apps/Etar/external/ex/camera2/utils/Android.bp:19:1: module "android-ex-camera2-utils" already defin
ed
       frameworks/ex/camera2/utils/Android.bp:15:1 <-- previous definition here
error: packages/apps/Etar/external/ex/camera2/public/Android.bp:19:1: module "android-ex-camera2" already defined
       frameworks/ex/camera2/public/Android.bp:15:1 <-- previous definition here
error: packages/apps/Etar/external/ex/camera2/portability/Android.bp:19:1: module "android-ex-camera2-portability" a
lready defined
       frameworks/ex/camera2/portability/Android.bp:15:1 <-- previous definition here
error: packages/apps/Etar/external/calendar/Android.bp:19:1: module "calendar-common" already defined
       frameworks/opt/calendar/Android.bp:15:1 <-- previous definition here
error: packages/apps/Etar/external/ex/framesequence/Android.bp:21:1: module "android-common-framesequence" already d
efined
       frameworks/ex/framesequence/Android.bp:17:1 <-- previous definition here
error: packages/apps/Etar/external/ex/common/Android.bp:22:1: module "android-common" already defined
       frameworks/ex/common/Android.bp:18:1 <-- previous definition here
error: packages/apps/Etar/external/ex/camera2/utils/tests/Android.bp:19:1: module "android-ex-camera2-utils-tests" a
lready defined
       frameworks/ex/camera2/utils/tests/Android.bp:15:1 <-- previous definition here
error: packages/apps/Etar/external/ex/camera2/portability/tests/Android.bp:19:1: module "android-ex-camera2-portabil
ity-tests" already defined
       frameworks/ex/camera2/portability/tests/Android.bp:15:1 <-- previous definition here
error: packages/apps/Etar/external/calendar/tests/Android.bp:19:1: module "CalendarCommonTests" already defined
       frameworks/opt/calendar/tests/Android.bp:15:1 <-- previous definition here
error: packages/apps/Etar/external/ex/framesequence/jni/Android.bp:21:1: module "libframesequence" already defined
       frameworks/ex/framesequence/jni/Android.bp:17:1 <-- previous definition here
error: packages/apps/Etar/external/ex/common/tests/Android.bp:19:1: module "AndroidCommonTests" already defined
       frameworks/ex/common/tests/Android.bp:15:1 <-- previous definition here
error: packages/apps/Etar/external/ex/framesequence/samples/FrameSequenceSamples/Android.bp:19:1: module "FrameSeque
nceSample" already defined
       frameworks/ex/framesequence/samples/FrameSequenceSamples/Android.bp:15:1 <-- previous definition here
17:38:32 soong bootstrap failed with: exit status 1

```

To fix it, just remove the Android.bp files:
```
rm packages/apps/Etar/external/ex/framesequence/samples/FrameSequenceSamples/Android.bp
rm packages/apps/Etar/external/ex/common/tests/Android.bp
rm packages/apps/Etar/external/ex/framesequence/jni/Android.bp
rm packages/apps/Etar/external/calendar/tests/Android.bp
rm packages/apps/Etar/external/ex/camera2/portability/tests/Android.bp
rm packages/apps/Etar/external/ex/camera2/utils/tests/Android.bp:19
rm packages/apps/Etar/external/ex/camera2/utils/tests/Android.bp
rm packages/apps/Etar/external/ex/common/Android.bp
rm packages/apps/Etar/external/ex/framesequence/Android.bp
rm packages/apps/Etar/external/calendar/Android.bp
rm packages/apps/Etar/external/ex/camera2/portability/Android.bp
rm packages/apps/Etar/external/ex/camera2/public/Android.bp
rm packages/apps/Etar/external/ex/camera2/utils/Android.bp
```

### 1/5/2022
On Arch, had to install ncurses5-compact-libs and multiarch glibc to build android-hal

--

Successfully synced with https instead of ssh. Added patches to be run before build, in apollon_patches

--

On ubuntu, install
```
sudo apt-get install libncurses5
```

--

Had to install ```libc6-dev-i386```

Find package for your distro here: [https://stackoverflow.com/questions/7412548/error-gnu-stubs-32-h-no-such-file-or-directory-while-compiling-nachos-source](https://stackoverflow.com/questions/7412548/error-gnu-stubs-32-h-no-such-file-or-directory-while-compiling-nachos-source)

--

When building droid-hal, there might be error like "dtb image must not be empty"
To resolve this, edit out/build-lineage_apollon.ninja, search for "kernel --base=", insert ```--dtb out/target/product/apollon/dtb.img``` after base argument

--

Successfully compiled android hal 22:21 PM

--

If it fails on something like:
```
+ cp out/target/product/apollon/kernel /home/fog/hadk/installroot/boot/kernel-4.19.113-lineageos-g903dc7159d03 4.19.113-lineageos-g903dc7159d03 4.19.113-lineageos-g903dc7159d03
cp: target '4.19.113-lineageos-g903dc7159d03' is not a directory
error: Bad exit status from /var/tmp/rpm-tmp.pywXjC (%install)
```
then I haven't updated dhd in repo, use this - [3ecaff4d1d96dd3a29e02b912ace38116413feb9](https://gitlab.com/FishFactoryHQ/apollon/hybris/droid-hal-device/-/commit/3ecaff4d1d96dd3a29e02b912ace38116413feb9)
